//
//  CustomAnimator.m
//  Animations
//
//  Created by Rene Cacheaux on 4/20/15.
//  Copyright (c) 2015 Mutual Mobile. All rights reserved.
//

#import "CustomAnimator.h"

@interface CustomAnimator ()
@property (nonatomic, strong) UIView *view;
@property (nonatomic, assign) CFTimeInterval duration;
@property (nonatomic, assign) CFTimeInterval startTimestamp;
@end

@implementation CustomAnimator

- (instancetype)initWithView:(UIView *)view duration:(CFTimeInterval)duration
{
  self = [super init];
  if (self) {
    _view = view;
    _duration = duration;
    _startTimestamp = 0;
  }
  return self;
}

- (void)animateNextFrame:(CADisplayLink *)displayLink
{
  
}

@end
