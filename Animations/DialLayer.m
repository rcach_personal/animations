//
//  DialLayer.m
//  Animations
//
//  Created by Rene Cacheaux on 4/20/15.
//  Copyright (c) 2015 Mutual Mobile. All rights reserved.
//

#import "DialLayer.h"

#import <UIKit/UIKit.h>

@implementation DialLayer

- (instancetype)init
{
  self = [super init];
  if (self) { }
  return self;
}

- (void)layoutSublayers
{
  [super layoutSublayers];
}

- (void)display
{
  [super display];
}

- (void)drawInContext:(CGContextRef)ctx
{
  [super drawInContext:ctx];
}

- (void)setContents:(id)contents
{
  [super setContents:(id)contents];
}

@end
