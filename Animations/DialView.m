//
//  DialView.m
//  Animations
//
//  Created by Rene Cacheaux on 4/20/15.
//  Copyright (c) 2015 Mutual Mobile. All rights reserved.
//

#import "DialView.h"

#import "DialLayer.h"

@implementation DialView

+ (Class)layerClass {
  return [DialLayer class];
}

- (instancetype)initWithFrame:(CGRect)frame
{
  NSLog(@"Initializing Dial...");
  self = [super initWithFrame:frame];
  if (self) {
    self.backgroundColor = [UIColor whiteColor];
  }
  return self;
}

- (void)layoutSubviews
{
  NSLog(@"Laying out Dial...");
  [super layoutSubviews];
  sleep(2);
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
  NSLog(@"Drawing Dial...");
  sleep(2);
}

@end
