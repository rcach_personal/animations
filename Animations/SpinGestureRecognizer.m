//
//  SpinGestureRecognizer.m
//  Animations
//
//  Created by Rene Cacheaux on 4/20/15.
//  Copyright (c) 2015 Mutual Mobile. All rights reserved.
//

#import "SpinGestureRecognizer.h"

#import <UIKit/UIGestureRecognizerSubclass.h>

@interface SpinGestureRecognizer ()
@property (nonatomic, assign) CGPoint originalTouchPosition;
@property (nonatomic, assign) CFTimeInterval lastTouchTime;
@end

@implementation SpinGestureRecognizer

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
  [super touchesBegan:touches withEvent:event];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
  [super touchesMoved:touches withEvent:event];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
  [super touchesEnded:touches withEvent:event];
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
  [super touchesCancelled:touches withEvent:event];
}

@end
