//
//  ViewController.m
//  Animations
//
//  Created by Rene Cacheaux on 4/20/15.
//  Copyright (c) 2015 Mutual Mobile. All rights reserved.
//

#import "ViewController.h"

#import "DialView.h"
#import "CustomAnimator.h"
#import "SpinGestureRecognizer.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UIButton *addDialButton;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *animateButtons;
@property (nonatomic, strong) DialView *dialView;
@property (nonatomic, strong) CustomAnimator *animator;
@end

@implementation ViewController

- (void)viewDidLoad
{
  [super viewDidLoad];
}

- (IBAction)addDial:(id)sender
{
//  CGFloat originX = self.view.bounds.size.width/2.0 - 80;
  
  self.addDialButton.enabled = false;
  for (UIButton *button in self.animateButtons) {
    button.enabled = true;
  }
}

- (IBAction)animateWithUIKit:(id)sender
{

}

- (IBAction)animateWithDisplayLink:(id)sender
{

}

- (IBAction)animateWithPOP:(id)sender
{

}

- (void)installTapGestureRecognizer {

}

- (void)handleTap {

}

- (void)installRotationGestureRecognizer {

}

- (void)handleRotation:(UIRotationGestureRecognizer *)gestureRecognizer {

}

- (void)installSpinGestureRecognizer {

}

- (void)handleSpin:(SpinGestureRecognizer *)gestureRecognizer {

}

@end
